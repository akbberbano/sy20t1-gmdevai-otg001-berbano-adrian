﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public GameObject explosion;

    int hp;

    // Start is called before the first frame update
    void Start()
    {
        hp = 100;
    }

    private void Update()
    {
        HealthCheck();
    }

    public void TakeDamage(int dmg)
    {
        hp -= dmg;
    }

    public int GetHealth()
    {
        return hp;
    }

    void HealthCheck()
    {
        if (hp <= 0)
        {
            GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
            Destroy(e, 1.5f);
            Destroy(gameObject);
        }
    }
}
