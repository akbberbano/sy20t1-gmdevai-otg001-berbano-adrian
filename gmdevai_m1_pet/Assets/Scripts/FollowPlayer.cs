﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;
    [SerializeField] float speed = 1f;
    [SerializeField] float rotationSpeed = 2.5f;

    void Start()
    {

    }

    void FixedUpdate()
    {
        Vector3 lookAtGoal = new Vector3(player.position.x, this.transform.position.y, player.position.z);
        Vector3 direction = lookAtGoal - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);
        
        if (Vector3.Distance(lookAtGoal, transform.position) > 2)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, player.transform.position, Time.deltaTime * speed); // transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }
}
