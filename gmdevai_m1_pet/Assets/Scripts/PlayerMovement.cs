﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    [SerializeField] float movementForce = 375f;

    [SerializeField] float direction = 5;
    [SerializeField] float movementSpeed = 5;

    void Start()
    {
        
    }

    void FixedUpdate()
    {
        // PLAYER STRAFE MOVEMENT

        // MOVE FORWARD
        if (Input.GetKey("w"))
            transform.Translate(new Vector3(0, 0, direction) * movementSpeed * Time.deltaTime);

        // MOVE BACK
        if (Input.GetKey("s"))
            transform.Translate(new Vector3(0, 0, -direction) * movementSpeed * Time.deltaTime);

        // MOVE LEFT
        if (Input.GetKey("a"))
            transform.Translate(new Vector3(-direction, 0, 0) * movementSpeed * Time.deltaTime);

        // MOVE RIGHT
        if (Input.GetKey("d"))
            transform.Translate(new Vector3(direction, 0, 0) * movementSpeed * Time.deltaTime);

        // PLAYER ROTATION

    }
}
