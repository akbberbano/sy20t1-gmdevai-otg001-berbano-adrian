﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelDirection : MonoBehaviour
{
    public Vector3 direction = new Vector3(0, 0, 4);
    float movementSpeed = 5;

    // Start is called before the first frame update
    void Start()
    {
        direction *= 0.01f;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.Translate(direction.normalized * movementSpeed * Time.deltaTime);
    }
}
