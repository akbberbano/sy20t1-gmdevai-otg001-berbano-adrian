﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent1 : AIControls
{
    // Wanders when the target is not within range
    // Pursues target when within range
    // Goes back to wander mode when target exits range

    public override void Update()
    {
        base.Update();

        if (targetDistance < range)
        {
            Pursue();
        }
        else
        {
            Wander();
        }
    }
}
