﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent2 : AIControls
{
    // Wanders when the target is not within range
    // Hides when target is within range and within line of sight
    // Goes back to wander mode when target exits range

    public override void Update()
    {
        base.Update();

        if (targetDistance < range)
        {
            Hide();
        }
        else
        {
            Wander();
        }
    }
}
