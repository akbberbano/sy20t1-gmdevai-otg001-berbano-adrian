﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent3 : AIControls
{
    // Wanders when the target is not within range
    // Evades when target is within range
    // Goes back to wander mode when target exits range

    public override void Update()
    {
        base.Update();

        if (targetDistance < range)
        {
            Evade();
        }
        else
        {
            Wander();
        }
    }
}
